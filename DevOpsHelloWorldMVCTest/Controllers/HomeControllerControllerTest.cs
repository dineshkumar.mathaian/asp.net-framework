﻿using Xunit;
using DevOpsHelloWorld.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DevOpsHelloWorld.Controllers.Tests
{
    public class HomeControllerControllerTest
    {
       
        [Fact()]
        public void AboutAboutTest()
        {
            // Arrange
            HomeController controller = new HomeController();
            // Act
            ViewResult result = controller.About() as ViewResult;
            // Assert
            Assert.Equal("Your application description page.", result.ViewBag.Message);
        }

       
    }
}