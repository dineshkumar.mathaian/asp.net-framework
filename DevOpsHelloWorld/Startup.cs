﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DevOpsHelloWorld.Startup))]
namespace DevOpsHelloWorld
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
